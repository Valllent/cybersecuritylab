
<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>The HTML5</title>
    <meta name="description" content="The HTML5">
    <meta name="author" content="SitePoint">
</head>

<body>
    <div class="wrapper"  style="width: 500px; margin: 0 auto; text-align: center">
        <form action="result.php" method="post">
            <p>Login:</p>
            <input type="text" name="login" style="width:300px;">
            <br>
            <p>Password:</p>
            <input type="password" name="password" style="width: 300px;">
            <br>
            <p>Type of encryption:</p>
            <select name="encryptionType">
                <option value="SHA256">SHA256</option>
                <option value="MD5">MD5</option>
            </select>
            <br><br>
            <input type="submit" width="300px" value="Submit" style="width:300px; height: 50px">
        </form>
    </div>
</body>
</html>